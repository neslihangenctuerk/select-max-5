This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## General
What you can do: Select 5 classes and calculate the amount of credits you'll earn with these. 
In the current base implementation: 
Classes are green in the current version, when selected they are set to red.
If 5 classes are selected a button appears with which the sum of the credits are calculated. Right now the selection disappears and only the result is displayed.

__next possible TODOs__:
+ Add styling - preferably via Styled Components
+ Make components for: __Section__, __Button__
+ Refactor __displayResults__ logic
+ Add __filter__ for credits



## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Which is licensed under: [LICENSE](https://github.com/facebook/create-react-app/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.

## Important
Please also read the __LICENSE.md__

