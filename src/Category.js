import React from 'react'

const Category = props => 
    <div>
        <button 
            className={props.selected ? 'red' : 'green'}
            onClick={props.onClick}>{props.title}</button>
        <div>{props.credits}</div>
        
    </div>


export default Category