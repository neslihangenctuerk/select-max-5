import React from 'react'
import Category from './Category'

class Categories extends React.Component {

    state = {
        selectedCategories: [],
        availableCategories: [
            {
                id: 0,
                title: 'Trigonometry',
                credits: 6
            },
            {
                id: 1,
                title: 'Calculus 1',
                credits: 6
            },
            {
                id: 2,
                title: 'Calculus 2',
                credits: 6
            },
            {
                id: 3,
                title: 'Statistics',
                credits: 4
            },
            {
                id: 4,
                title: 'Quantum Physics 1',
                credits: 8
            },
            {
                id: 5,
                title: 'Intro to Python',
                credits: 4
            },
            {
                id: 6,
                title: 'Intro to C++',
                credits: 6
            }
        ],
        totalCredits: null,
        displayResults: false
    }

    handleCalculation = () => {
        this.setState((state) => {
            const sumOfCredits = state.selectedCategories.map(selectedCategory => selectedCategory.credits)
                                                                .reduce((sum, credit) => sum + credit, 0)
            return {
                totalCredits: sumOfCredits,
                displayResults: true
            }
        })
    }

    handleSelection = (selection) => {
        console.log(selection)
        
        this.setState((state) => {
            const isAlreadySelected = state.selectedCategories.find(cat => cat.id == selection.id) ? true : false
            const lenOfSelections = state.selectedCategories.length

            // if not selected before :
            if (!isAlreadySelected && lenOfSelections < 5) {
                return {
                    selectedCategories: [...state.selectedCategories, selection]
                }
                
            // if already selected before:
            } else {
                const updatedSelection = [...state.selectedCategories].filter(cat => cat.id !== selection.id)
                return {
                    selectedCategories: updatedSelection
                }
            }
           
        })
    }

    render () {
        const lenOfSelections = this.state.selectedCategories.length
        return (
            <div>
                <h3>Categories Comp</h3>
                {
                    !this.state.displayResults && 
                        this.state.availableCategories.map(category => 
                            <Category
                                selected={this.state.selectedCategories.find(cat => cat.id == category.id) ? true : false}
                                key={category.id}
                                id={category.id}
                                title={category.title}
                                credits={category.credits}
                                onClick={() =>this.handleSelection(category)}
                            />
                        )
                }

                {
                    (!this.state.displayResults && lenOfSelections == 5) &&
                        <button onClick={this.handleCalculation}>Calculate Credits</button>
                }

                {
                    this.state.totalCredits && 
                        <div>Your total credits with this selection: {this.state.totalCredits}</div>
                }
                
            </div>
        )
    }
}

export default Categories